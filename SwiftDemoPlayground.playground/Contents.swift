import UIKit

/*  Employee class */
class Employee: NSObject {
    var id: Int
    var firstName: String
    var lastName: String
    var email: String
    var salary: Double
    
    init(_ id: Int, _ firstName: String, _ lastName: String, _ email: String, _ salary: Double) {
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.salary = salary
    }
    
    func formatSalary() -> String {
        return String("$\(self.salary)")
    }
    
    public override var description: String {
        return String("\(self.lastName), \(self.firstName) <\(self.email)>: \(self.formatSalary())")
    }
}

// constant
let lastName = "Doe"

var e1 = Employee(1, "John", lastName, "john.doe@nje.hu", 2000)
var e2 = Employee(1, "Lisa", lastName, "liza.doe@nje.hu", 2500)
var e3 = Employee(1, "Jeff", lastName, "j.doe@nje.hu", 1560)
var e4 = Employee(1, "Harry", lastName, "h.doe@nje.hu", 3400)

// print
print("Employees:")
print(e1.description)
print(e2.description)
print(e3.description)
print(e4.description)

// increase salary
e1.salary += 100
e2.salary *= 2
// decrease salary
e3.salary -= 50
e4.salary /= 2

if e2.salary > e1.salary && e2.salary > e3.salary && e2.salary > e4.salary  {
    print("Lisa has the top salary")
}

// print
print("\n\nNew salaries:")
print(e1.description)
print(e2.description)
print(e3.description)
print(e4.description)
